//
//  WebViewController.m
//  Vesti
//
//  Created by Kurs on 2/4/17.
//  Copyright © 2017 kurs. All rights reserved.
//

#import "WebViewController.h"

@interface WebViewController () <UIWebViewDelegate>
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *spinnerView;
@end


@implementation WebViewController
#pragma mark - View lifecycle
-(void) viewDidLoad {
    [super viewDidLoad];
    
    NSURL *url = [NSURL URLWithString:self.url];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [self.webView loadRequest:request];
}

#pragma mark UIWebViewDelegate
// mali spinner u vrhu telefona
-(void)webViewDidStartLoad:(UIWebView *)webView {
    // ova tri dole su ista, samo je pristup razlicit
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    // [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    // UIApplication.sharedApplication.networkActivityIndicatorVisible = YES;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    // ovde sam rekao spinner da stane
    [self.spinnerView stopAnimating];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}
@end
