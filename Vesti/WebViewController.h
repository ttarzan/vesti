//
//  WebViewController.h
//  Vesti
//
//  Created by Kurs on 2/4/17.
//  Copyright © 2017 kurs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WebViewController : UIViewController
@property (strong, nonatomic) NSString *url;
@end
