//
//  Article.m
//  Vesti
//
//  Created by Kurs on 2/3/17.
//  Copyright © 2017 kurs. All rights reserved.
//

#import "Article.h"

@implementation Article

#pragma mark - Designated Initializer
- (instancetype)initWithDictionary:(NSDictionary *)dictionary {
    if(self = [super init]) {
        // jedan nacin parsiranja
        self.title = [dictionary objectForKey:@"title"];
        // drugi nacin parsiranja
        self.desc = dictionary[@"description"];
        self.imageURl = dictionary[@"imageUrl"];
        self.region = dictionary[@"region"];
        self.url = dictionary[@"url"];
        self.portal = dictionary[@"portal"];
    }
    return self;
}
@end
