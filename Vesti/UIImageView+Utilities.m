//
//  UIImageView+Utilities.m
//  Vesti
//
//  Created by Kurs on 2/4/17.
//  Copyright © 2017 kurs. All rights reserved.
//

#import "UIImageView+Utilities.h"

@implementation UIImageView (Utilities)

#pragma mark - Public API
- (void)loadImageFromURL:(NSString *)url {
    // kreirali smo que i dali mu naziv
    dispatch_queue_t downloadQueue = dispatch_queue_create("ImageDownloader", NULL);
    // asinhrono download tu sliku
    dispatch_async(downloadQueue, ^{
        NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:url]];
        UIImage *image = [[UIImage alloc]initWithData:data];
        
        // kada zavrsis ainhrono se vrati i setuj tu sliku
        dispatch_async(dispatch_get_main_queue(), ^{
            self.image = image;
        });
    });
}

@end
